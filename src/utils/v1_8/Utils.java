package utils.v1_8;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerListHeaderFooter;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import net.minecraft.server.v1_8_R3.PlayerConnection;

import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;

public class Utils {
	 public static boolean isItemInHand(Player player, Material m)
	 {
		 if(player.getItemInHand() == null)
		 {
			 return false;
		 } else {
			 return player.getItemInHand().getType() == m;
		 }
	 }
	 
	  public static void sendTablist(Player player, String header, String footer)
	  {
	    if (header == null) {
	      header = "";
	    }
	    header = ChatColor.translateAlternateColorCodes('&', header);
	    if (footer == null) {
	      footer = "";
	    }
	    footer = ChatColor.translateAlternateColorCodes('&', footer);
	    
	    header = header.replaceAll("%player%", player.getDisplayName());
	    footer = footer.replaceAll("%player%", player.getDisplayName());
	    
	    PlayerConnection connection = ((CraftPlayer)player).getHandle().playerConnection;
	    IChatBaseComponent tabTitle = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + header + "\"}");
	    IChatBaseComponent tabFoot = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + footer + "\"}");
	    PacketPlayOutPlayerListHeaderFooter headerPacket = new PacketPlayOutPlayerListHeaderFooter(tabTitle);
	    try
	    {
	      Field field = headerPacket.getClass().getDeclaredField("b");
	      field.setAccessible(true);
	      field.set(headerPacket, tabFoot);
	    }
	    catch (Exception e)
	    {
	      e.printStackTrace();
	    }
	    finally
	    {
	      connection.sendPacket(headerPacket);
	    }
	  }
		  
	 
	 public static List<String> whitelist = new ArrayList<String>();
	 
	  public static String fixedCaps(String message)
	  {
		  whitelist.clear();
		  whitelist.add("T3CH");
	    String[] parts = message.split(" ");
	    boolean allowedCaps = false;
	    for (int i = 0; i < parts.length; i++)
	    {
	      boolean whitelisted = false;
	      for (String whiteWord : whitelist) {
	        if (whiteWord.equalsIgnoreCase(parts[i]))
	        {
	          whitelisted = true;
	          allowedCaps = true;
	          break;
	        }
	      }
	      if (!whitelisted)
	      {
	        if (!allowedCaps)
	        {
	          char firstChar = parts[i].charAt(0);
	          parts[i] = (firstChar + parts[i].toLowerCase().substring(1));
	        }
	        else
	        {
	          parts[i] = parts[i].toLowerCase();
	        }
	        allowedCaps = (!parts[i].endsWith(".")) && (!parts[i].endsWith("!"));
	      }
	    }
	    return StringUtils.join(parts, " ").replaceAll("%", " percent");
	  }
	  
	  public static void spawnRandomFirework(Location location)
		 {
			  RF.spawnRandomFirework(location);
		 }
		
	  public static boolean isLeftClick(Action a)
	  {
		  return a == Action.LEFT_CLICK_AIR || a == Action.LEFT_CLICK_BLOCK;
	  }
	  
	  public static boolean isRightClick(Action a)
	  {
		  return a == Action.RIGHT_CLICK_AIR || a == Action.RIGHT_CLICK_BLOCK;
	  }
	  
		
	  public static void sendActionBar(Player p, String message)
	  {
	    IChatBaseComponent icbc = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + message + "\"}");
	    PacketPlayOutChat bar = new PacketPlayOutChat(icbc, (byte)2);
	    ((CraftPlayer)p).getHandle().playerConnection.sendPacket(bar);
	  }
		public static void sendTitle(Player player, String title)
		{
			sendTitle(player, title, " ");
		}
		
		public static void sendTitle(Player player, String title, String subtitle)
		{
			fsendTitle(player, 5, 20, 10, title, subtitle);

		}
		
		public static void sendTitle(Player player, String title, Integer fadein)
		{
			fsendTitle(player, fadein, 20, 10, title, " ");

		}
		
		public static void sendTitle(Player player, String title, String subtitle, Integer fadein)
		{
			fsendTitle(player, fadein, 20, 10, title, subtitle);

		}
		
		public static void sendTitle(Player player, String title, Integer fadeout, Integer fadein)
		{
			fsendTitle(player, fadein, 20, fadeout, title, " ");

		}
		
		public static void sendTitle(Player player, String title, String subtitle, Integer fadeout, Integer fadein)
		{
			fsendTitle(player, fadein, 20, fadeout, title, subtitle);

		}
		
		public static void sendTitle(Player player, String title, Integer fadeout, Integer stay, Integer fadein)
		{
			fsendTitle(player, fadein, stay, fadeout, title, " ");
		}
		
		  public static void sendTitle(Player player, String title, String subtitle, Integer fadeIn, Integer stay, Integer fadeOut)
		  {
		    PlayerConnection connection = ((CraftPlayer)player).getHandle().playerConnection;
		    
		    PacketPlayOutTitle packetPlayOutTimes = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TIMES, null, fadeIn.intValue(), stay.intValue(), fadeOut.intValue());
		    connection.sendPacket(packetPlayOutTimes);
		    if (subtitle != null)
		    {
		      subtitle = subtitle.replaceAll("%player%", player.getDisplayName());
		      subtitle = ChatColor.translateAlternateColorCodes('&', subtitle);
		      IChatBaseComponent titleSub = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + subtitle + "\"}");
		      PacketPlayOutTitle packetPlayOutSubTitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, titleSub);
		      connection.sendPacket(packetPlayOutSubTitle);
		    }
		    if (title != null)
		    {
		      title = title.replaceAll("%player%", player.getDisplayName());
		      title = ChatColor.translateAlternateColorCodes('&', title);
		      IChatBaseComponent titleMain = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + title + "\"}");
		      PacketPlayOutTitle packetPlayOutTitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, titleMain);
		      connection.sendPacket(packetPlayOutTitle);
		    }
		  }
		  
		  public static void fsendTitle(Player player, Integer fadeIn, Integer stay, Integer fadeOut, String title, String subtitle)
		  {
		    PlayerConnection connection = ((CraftPlayer)player).getHandle().playerConnection;
		    
		    PacketPlayOutTitle packetPlayOutTimes = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TIMES, null, fadeIn.intValue(), stay.intValue(), fadeOut.intValue());
		    connection.sendPacket(packetPlayOutTimes);
		    if (subtitle != null)
		    {
		      subtitle = subtitle.replaceAll("%player%", player.getDisplayName());
		      subtitle = ChatColor.translateAlternateColorCodes('&', subtitle);
		      IChatBaseComponent titleSub = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + subtitle + "\"}");
		      PacketPlayOutTitle packetPlayOutSubTitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, titleSub);
		      connection.sendPacket(packetPlayOutSubTitle);
		    }
		    if (title != null)
		    {
		      title = title.replaceAll("%player%", player.getDisplayName());
		      title = ChatColor.translateAlternateColorCodes('&', title);
		      IChatBaseComponent titleMain = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + title + "\"}");
		      PacketPlayOutTitle packetPlayOutTitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, titleMain);
		      connection.sendPacket(packetPlayOutTitle);
		    }
		  }
		
		
}
