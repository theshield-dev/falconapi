package com.tedthetechie.falcon.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class ClockTickEvent extends Event {

	public ClockTickEvent() {
	
	}
	
	private static final HandlerList handlers = new HandlerList();
	 
    public HandlerList getHandlers() {
        return handlers;
    }
 
    public static HandlerList getHandlerList() {
        return handlers;
    }

}
