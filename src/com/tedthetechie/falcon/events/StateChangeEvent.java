package com.tedthetechie.falcon.events;

import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.tedthetechie.falcon.resources.GameState;

public class StateChangeEvent extends Event implements Cancellable {

	private GameState before;
	private GameState after;
	
	public StateChangeEvent(GameState before, GameState after) {
		cancelled = false;
		this.before = before;
		this.after = after;
	}
	
	public GameState getOldGameState() {
		return before;
	}
	
	public GameState getNewGameState() {
		return after;
	}
	
	
	private static final HandlerList handlers = new HandlerList();
	private static boolean cancelled;
	 
    public HandlerList getHandlers() {
        return handlers;
    }
 
    public static HandlerList getHandlerList() {
        return handlers;
    }

	@Override
	public boolean isCancelled() {
		// TODO Auto-generated method stub
		return cancelled;
	}

	@Override
	public void setCancelled(boolean arg0) {
		// TODO Auto-generated method stub
		cancelled = arg0;
	}
}
