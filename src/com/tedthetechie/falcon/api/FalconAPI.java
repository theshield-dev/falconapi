package com.tedthetechie.falcon.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.tedthetechie.falcon.resources.GameState;
import com.tedthetechie.falcon.resources.Kit;
import com.tedthetechie.falcon.resources.Team;

public class FalconAPI {

	private static int LOBBY = 30;
	
	private static int GAME = 300;
	
	private static int END = 10;
	
	private static GameState STATE = GameState.Lobby;
	
	private static boolean RUNTIMER = false;
	
	private static boolean KITS = false;
	
	private static List<Kit> kits = new ArrayList<Kit>();
	
	private static Map<Player, Kit> saved = new HashMap<Player, Kit>();
	
	private static Kit defaultKit = null;
	
	private static int MINIMUM_PLAYERS = 1;
	
	private static String JOIN_MESSAGE = "%displayname% �ejoined�7.";
	
	private static String QUIT_MESSAGE = "%displayname% �eleft�7.";
	
	private static List<Player> players = new ArrayList<Player>();
	
	private static List<Player> alive = new ArrayList<Player>();
	
	private static Boolean BUILDING = false;
	
	private static boolean ITEM_DROPS = false;
	
	private static List<Team> TEAMS = new ArrayList<Team>();
	
	private static String prefix = "�4�l* �r";
	
	private static Map<Kit, Team> kitteamonly = new HashMap<Kit, Team>();
	
	public static void setKitTeamRestriction(Kit kit, Team team) {
		if(kitteamonly.containsKey(kit)) {
			kitteamonly.remove(kit);
		}
		kitteamonly.put(kit, team);
	}
	
	public static boolean doesKitHaveRestriction(Kit kit) {
		return kitteamonly.containsKey(kit);
	}
	
	public static Team getTeamForKit(Kit kit) {
		if(!doesKitHaveRestriction(kit)) {
			return null;
		} else {
			return kitteamonly.get(kit);
		}
	}
	
	public static void removeKitTeamRestriction(Kit kit) {
		if(kitteamonly.containsKey(kit)) {
			kitteamonly.remove(kit);
		}
	}
	
	public static void setPrefix(String prefixx) {
		prefix = prefixx;
	}
	
	public static void sendMessage(Player player, String msg) {
		player.sendMessage(prefix + msg);
	}
	
	public static void addTeam(Team team) {
		
		TEAMS.add(team);
	}
	
	public static void removeTeam(Team team) {
		
	}
	
	
	public static List<Team> getTeams() {
		return TEAMS;
	}
	
	public static boolean isInTeam(Player player) {
		Boolean inteam = false;
		for(Team t : TEAMS) {
			if(t.containsPlayer(player)) {
				inteam = true;
			}
		}
		return inteam;
	}
	
	public static Team getTeam(Player player) {
		Team inteam = null;
		for(Team t : TEAMS) {
			if(t.containsPlayer(player)) {
				inteam = t;
			}
		}
		return inteam;
	}
	
	public static void clearKits() {
		saved.clear();
		kits.clear();
	}
	
	public static void setBuilding(boolean b) {
		BUILDING = b;
	}
	
	public static boolean eventCanBuild() {
		return !BUILDING;
	}
	
	public static void setDrops(boolean b) {
		ITEM_DROPS = b;
	}
	
	public static boolean doDrops() {
		return ITEM_DROPS;
	}
	
	public static void removeParticipant(Player player) {
		if(players.contains(player)) {
			players.remove(player);
		}
	}
	
	public static void resetKit(Player player) {
		if(saved.containsKey(player)) {
			saved.remove(player);
		}
	}
	
	public static void updateKit(Player player) {
		if(saved.containsKey(player)) {
			player.getInventory().setContents(saved.get(player).getInventoryContents());
			player.getInventory().setArmorContents(saved.get(player).getArmorContents());
			player.updateInventory();
		} else {
			if(defaultKit != null){
				player.getInventory().setContents(defaultKit.getInventoryContents());
				player.getInventory().setArmorContents(defaultKit.getArmorContents());
				player.updateInventory();
			}
		}
	}
	
	public static void setKit(Player player, Kit kit) {
		if(!saved.containsKey(player)) {
			saved.put(player, kit);
		} else {
			resetKit(player);
			setKit(player, kit);
		}
	}
	
	public static void setDefaultKit(Kit kit) {
		defaultKit = kit;
	}
	
	public static List<Player> getAlive() {
		return alive;
	}
	
	public static void setAlive(Player player, Boolean b) {
		if(b) {
			if(!alive.contains(player)) {
				alive.add(player);
			}	 
		} else {
			if(alive.contains(player)) {
				alive.remove(player);
			}
		}
	}
	
	public static void addParticipant(Player player) {
		if(!players.contains(player)) {
			players.add(player);
		}
	}
	
	public static List<Player> getParticipants() {
		return players;
	}
	
	public static void setJoinMessage(String i) {
		JOIN_MESSAGE = i;
	}
	
	public static String getJoinMessage(Player player) {
		return JOIN_MESSAGE.replaceAll("%displayname%", player.getDisplayName());
	}
	
	public static String getQuitMessage(Player player) {
		return QUIT_MESSAGE.replaceAll("%displayname%", player.getDisplayName());
	}
	
	
	public static String getJoinMessage() {
		return JOIN_MESSAGE;
	}
	
	public static String getQuitMessage() {
		return QUIT_MESSAGE;
	}
	
	public static void setQuitMessage(String i) {
		QUIT_MESSAGE = i;
	}
	
	public static GameState getState() {
		return STATE;
	}
	
	public static void setState(GameState state) {
		STATE = state;
	}
	
	public static int getMinimumPlayers() {
		return MINIMUM_PLAYERS;
	}
	
	public static void setRequiredPlayers(int i) {
		MINIMUM_PLAYERS = i;

	
	}
	public static void setLobbyTimer(int i) {
		LOBBY = i;
	}
	
	public static int getLobbyTimer() {
		return LOBBY;
	}
	
	public static void setGameTimer(int i) {
		GAME = i;
	}
	
	public static int getGameTimer() {
		return GAME;
	}
	
	public static void setEndTimer(int i) {
		END = i;
	}
	
	public static int getEndTimer() {
		return END;
	}
	public static boolean runLobbyTimer() {
		return RUNTIMER;
	}
	
	public static boolean runGameTimer() {
		return RUNTIMER;
	}
	
	public static boolean runEndTimer() {
		return RUNTIMER;
	}
	
	public static void runTimer(boolean b) {
		RUNTIMER = b;
	}
	
	/*
	 * 
	 * KITS
	 * 
	 */
	
	public static void addKit(Kit kit) {
		kits.add(kit);
	}
	
	public static void removeKit(Kit kit) {
		kits.remove(kit);
	}
	
	public static List<Kit> getKits() {
		return kits;
	}
	
	public static void setKits(Boolean arg0) {
		KITS = arg0;
	}
	
	public static boolean areKitsEnabled() {
		return KITS;
	}
	
	public static boolean setupKits() {
		if(KITS) {
			for(Player player : Bukkit.getOnlinePlayers()) {
				if(saved.containsKey(player)) {
					player.getInventory().setContents(saved.get(player).getInventoryContents());
					player.getInventory().setArmorContents(saved.get(player).getArmorContents());
					player.updateInventory();
				} else {
					player.getInventory().setContents(defaultKit.getInventoryContents());
					player.getInventory().setArmorContents(defaultKit.getArmorContents());
					player.updateInventory();
				}
			}
			
			return true;
		} else {
			return false;
		}
	}
}
