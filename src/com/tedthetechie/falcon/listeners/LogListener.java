package com.tedthetechie.falcon.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.tedthetechie.falcon.api.FalconAPI;

public class LogListener implements Listener {

	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		event.setJoinMessage(FalconAPI.getJoinMessage(player));
		player.getInventory().clear();
		player.setLevel(0);
		player.setExp(0);
		FalconAPI.addParticipant(player);
		player.updateInventory();
		if(Bukkit.getOnlinePlayers().size() >= FalconAPI.getMinimumPlayers()) {
			if(!FalconAPI.runLobbyTimer()) {
				FalconAPI.runTimer(true);
			}
		}
	}
	
	@EventHandler
	public void onDamage(EntityDamageByEntityEvent event) {
		if(event.getEntity() instanceof Player) { 
			Player hit = (Player) event.getEntity();
			if(event.getDamager() instanceof Player) {
				Player damager = (Player) event.getDamager();
				if(FalconAPI.isInTeam(hit) && FalconAPI.isInTeam(damager)) {
					if(FalconAPI.getTeam(hit) == FalconAPI.getTeam(damager)) {
						if(FalconAPI.getTeam(hit).doFriendlyFire() == false) {
							event.setCancelled(true);
						}
					}
				}
			} else if(event.getDamager() instanceof Projectile) {
				Projectile p = (Projectile) event.getDamager();
				if(p.getShooter() instanceof Player) {
					Player damager = (Player) p.getShooter();
					if(FalconAPI.isInTeam(hit) && FalconAPI.isInTeam(damager)) {
						if(FalconAPI.getTeam(hit) == FalconAPI.getTeam(damager)) {
							if(FalconAPI.getTeam(hit).doFriendlyFire() == false) {
								event.setCancelled(true);
							}
						}
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		FalconAPI.removeParticipant(player);
		FalconAPI.setAlive(player, false);
		FalconAPI.resetKit(player);
		event.setQuitMessage(FalconAPI.getQuitMessage(player));
	}
}
