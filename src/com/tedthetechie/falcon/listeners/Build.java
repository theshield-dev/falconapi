package com.tedthetechie.falcon.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerDropItemEvent;

import com.tedthetechie.falcon.api.FalconAPI;
import com.tedthetechie.falcon.resources.GameState;

 
public class Build implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void onBuild1(BlockPlaceEvent event) {
		if(FalconAPI.getState() == GameState.Game) {
			event.setCancelled(FalconAPI.eventCanBuild()); 
		} else {
			event.setCancelled(true);
		}
	}
	 
	@EventHandler(priority = EventPriority.LOWEST)
	public void onBuild2(BlockBreakEvent event) {
		if(FalconAPI.getState() == GameState.Game) {
			event.setCancelled(FalconAPI.eventCanBuild());
		} else {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onDrop(PlayerDropItemEvent event) {
		if(!FalconAPI.doDrops()) {
			event.setCancelled(true);
		}
	}
}
