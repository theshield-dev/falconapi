package com.tedthetechie.falcon.kitmenu;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import utils.v1_8.Utils;

import com.tedthetechie.falcon.api.FalconAPI;
import com.tedthetechie.falcon.resources.Kit;

public class KitMenu implements Listener {

	private static String INV = "�cKit Selector"; 
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onCmd(PlayerCommandPreprocessEvent event) { 
		if(event.getMessage().toLowerCase().startsWith("/kit")) {
			event.setCancelled(true);
			Player player = event.getPlayer();
			if(!FalconAPI.areKitsEnabled()) {
				Utils.sendTitle(player, "�1 �r", "�cThis feature is disabled", 0, 20, 0);
				return;
			}
			Inventory kitinv = Bukkit.createInventory(null, 27, INV);
			Integer kitno = 0;
			for(Kit k : FalconAPI.getKits()) {
				kitno++;
				ItemStack kit = new ItemStack(k.getMaterial(), kitno);
				ItemMeta kitm = kit.getItemMeta();
				kitm.setDisplayName(k.getDisplayName());
				List<String> lore = new ArrayList<String>();
				lore.add("�cArmor�8:");
				int armorcount = 0;
				for(ItemStack stack : k.getArmorContents()) {
					if(stack != null) {
						if(armorcount > 5) return;
						armorcount++;
						String s = Utils.fixedCaps(Material.getMaterial(stack.getTypeId()).toString()).replaceAll("_", " ");
						lore.add("�a�l" + s);
					} 
					
				}
				int invcount = 0;
				for(ItemStack stack : k.getInventoryContents()) {
					if(stack != null) {
						if(invcount > 6) {
							invcount++;
							if(invcount >= k.getInventoryContents().length) {
								lore.add("�7... and �a" + invcount + "�7 more items..");
							}
							return;
						}
						invcount++;
						String s = Utils.fixedCaps(Material.getMaterial(stack.getTypeId()).toString()).replaceAll("_", " ");
						lore.add("�a�l" + s);
					} 
					
				}
				lore.add("�7--------------");
				kitm.setLore(lore);
				kit.setItemMeta(kitm);
				kitinv.addItem(kit);
			}
			player.openInventory(kitinv);
		}
	}
	
	
	@EventHandler
	public void onKit(InventoryClickEvent event) {
		if(event.getInventory().getName().equalsIgnoreCase(INV)) {
			if(event.getCurrentItem() != null) {
				event.setCancelled(true);
				String name = event.getCurrentItem().getItemMeta().getDisplayName();
				Kit kit = null;
				for(Kit k : FalconAPI.getKits()) {
					if(k.getDisplayName().equalsIgnoreCase(name)) {
						kit = k;
					}
				}
				if(kit == null) {
					Utils.sendTitle((Player)event.getWhoClicked(), "�1 �r", "�cKit not Found", 0, 2, 0);
				} else {
					if(FalconAPI.doesKitHaveRestriction(kit)) {
						Player player = (Player) event.getWhoClicked();
						if(!FalconAPI.isInTeam(player)) {
							event.setCancelled(true);
							Utils.sendActionBar(player, "�c�lYOU MUST BE ON TEAM �a�l" + FalconAPI.getTeamForKit(kit).getDisplayName() + "�c�l TO USE THIS KIT");
						} else {
							if(FalconAPI.getTeam(player) != FalconAPI.getTeamForKit(kit)) {
								event.setCancelled(true);
								Utils.sendActionBar(player, "�c�lYOU MUST BE ON TEAM �a�l" + FalconAPI.getTeamForKit(kit).getDisplayName() + "�c�l TO USE THIS KIT");
						
							} else {
								FalconAPI.setKit((Player)event.getWhoClicked(), kit);
								Utils.sendActionBar(player, "�a�lKIT SELECTED");
							}
						}
						return;
					}
					Player player = (Player) event.getWhoClicked();	
					player.closeInventory();
					FalconAPI.setKit((Player)event.getWhoClicked(), kit);
					Utils.sendActionBar(player, "�a�lKIT SELECTED");
				}
			}
		}
	}
}
