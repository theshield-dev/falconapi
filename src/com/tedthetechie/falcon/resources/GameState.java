package com.tedthetechie.falcon.resources;

public enum GameState {

	Lobby, Game, Ending
}
