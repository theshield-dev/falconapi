package com.tedthetechie.falcon.resources;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Kit {

	
	private ItemStack[] inventory;
	
	private ItemStack[] armor;
	
	private String name;
	
	private String displayname;
	
	private Material display;
	
	public Kit(String name, String displayname, ItemStack[] inventory, ItemStack[] armor, Material display) {
		this.inventory = inventory;
		this.armor = armor;
		this.name = name;
		this.displayname = displayname;
		this.display = display;
	}
	
	public Material getMaterial() {
		return display;
	}
	
	public ItemStack[] getArmorContents() {
		return armor;
	}
	
	public ItemStack[] getInventoryContents() {
		return inventory;
	}
	
	public String getName() {
		return name;
	}
	
	public String getDisplayName() {
		return displayname;
	}
	
	public void delete() {
		displayname = "�cNon-Existant";
		name = "Non-Existant";
		armor = null;
		inventory = null;
	}
}
