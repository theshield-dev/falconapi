package com.tedthetechie.falcon.resources;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import com.tedthetechie.falcon.api.FalconAPI;

public class Team {

	private String name;
	private String displayname;
	private Boolean friendlyfire;
	private List<Player> players;
	
	public Team(String name, String displayname, Boolean friendlyfire) {
		this.name = name;
		this.displayname = displayname;
		players = new ArrayList<Player>();
		this.friendlyfire = friendlyfire;
		register();
	}
	
	public String getName() {
		return name;
	}
	
	public String getDisplayName() {
		return displayname;
	}
	
	public Boolean doFriendlyFire() {
		return friendlyfire;
	}
	
	public boolean containsPlayer(Player player) {
		return players.contains(player);
	}
	
	public void removePlayer(Player player) {
		if(containsPlayer(player)) {
			players.remove(player);
			FalconAPI.sendMessage(player, "�7You left team �a" + displayname + "�7.");
		}
	}
	
	public void addPlayer(Player player) {
		if(!containsPlayer(player)) {
			players.add(player);
			FalconAPI.sendMessage(player, "�7You joined team �a" + displayname + "�7.");
		}
	}
	
	public int size() {
		return players.size();
	}
	
	public void remove() {
		for(Player player : players) {
			FalconAPI.sendMessage(player, "�7You were kicked from team �a" + displayname + "�7.");
		}
		players.clear();
		FalconAPI.removeTeam(this);
	}
	
	private void register() {
		FalconAPI.addTeam(this);
	}
}
