package com.tedthetechie.falcon;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import utils.v1_8.Utils;

import com.tedthetechie.falcon.api.FalconAPI;
import com.tedthetechie.falcon.events.ClockTickEvent;
import com.tedthetechie.falcon.events.StateChangeEvent;
import com.tedthetechie.falcon.kitmenu.KitMenu;
import com.tedthetechie.falcon.listeners.Build;
import com.tedthetechie.falcon.listeners.LogListener;
import com.tedthetechie.falcon.resources.GameState;
import com.tedthetechie.falcon.spectator.SpectatorAPI;

public class Falcon extends JavaPlugin implements Listener  {

	
	private static Plugin plugin = null;
	
	public void onEnable() {
		plugin = this;
		getServer().getPluginManager().registerEvents(new LogListener(), this);
		getServer().getPluginManager().registerEvents(new KitMenu(), this);
		getServer().getPluginManager().registerEvents(new SpectatorAPI(), this);
		getServer().getPluginManager().registerEvents(new Build(), this);
		getServer().getPluginManager().registerEvents(this, this);
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			public void run() {
				for(Player p : Bukkit.getOnlinePlayers()) {
					if(!FalconAPI.getAlive().contains(p) && FalconAPI.getState() == GameState.Game) {
						SpectatorAPI.setSpectator(p);
					}
				}
				if(FalconAPI.runGameTimer()) {
					Bukkit.getPluginManager().callEvent(new ClockTickEvent());
					if(FalconAPI.getState() == GameState.Lobby) {
						if(FalconAPI.getLobbyTimer() <= 0) {
							Bukkit.getPluginManager().callEvent(new StateChangeEvent(GameState.Lobby, GameState.Game));
							FalconAPI.setState(GameState.Game);
						} else {
							FalconAPI.setLobbyTimer(FalconAPI.getLobbyTimer()-1);							
						}
					} else if(FalconAPI.getState() == GameState.Game) {
						if(FalconAPI.getGameTimer() <= 0) {
							Bukkit.getPluginManager().callEvent(new StateChangeEvent(GameState.Game, GameState.Ending));
							FalconAPI.setState(GameState.Ending);
						} else {
							FalconAPI.setGameTimer(FalconAPI.getGameTimer()-1);
						}
					} else if(FalconAPI.getState() == GameState.Ending) {
						if(FalconAPI.getEndTimer() > 0) {
							FalconAPI.setEndTimer(FalconAPI.getEndTimer()-1);							
						} 
					}
					 
				}
			}
		}, 20L, 20L);
	}
	
	public static Plugin getPlugin() {
		return plugin;
	}
	
	@EventHandler
	public void onCmd(PlayerCommandPreprocessEvent event) {
		if(event.getMessage().toLowerCase().startsWith("/falcon") || event.getMessage().toLowerCase().startsWith("/gameapi")) {
			event.setCancelled(true);
			Player player = event.getPlayer();
			Utils.sendTitle(player, "�cFalcon", "�fVersion �51.13.2", 1, 3, 1);
		}
	}
}
