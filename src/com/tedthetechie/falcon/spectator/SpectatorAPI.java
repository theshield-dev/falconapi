package com.tedthetechie.falcon.spectator;


import java.text.DecimalFormat;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.vehicle.VehicleDamageEvent;
import org.bukkit.event.vehicle.VehicleEnterEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.tedthetechie.falcon.api.FalconAPI;

public class SpectatorAPI implements Listener {

	
	public static void setSpectator(Player player) {
		player.getInventory().clear();
		player.setAllowFlight(true);
		
		ItemStack nearest = new ItemStack(Material.COMPASS, 1);
		ItemMeta meta = nearest.getItemMeta();
		Player closest = null;
		double distance = 999999999.00;
		for(Player p : Bukkit.getOnlinePlayers()) {
			if(p.getWorld().equals(player.getWorld()) && FalconAPI.getAlive().contains(p)) {
				double d = p.getLocation().distance(player.getLocation());	
				if(d < distance) {
					closest = p;
					distance = d;
				}
			}
		}
		if(closest == null) {
			meta.setDisplayName("�fTarget�8: �cNot Found �fDistance�8: �cUnknown");
		} else {
			DecimalFormat f = new DecimalFormat("#.##");
			meta.setDisplayName("�fTarget�8: �a" + closest.getDisplayName() + " �fDistance�8: �e" + f.format(distance));
			player.setCompassTarget(closest.getLocation());
		}
		for(Player p : Bukkit.getOnlinePlayers()) {
			p.hidePlayer(player);
		}
		nearest.setItemMeta(meta);
		player.getInventory().setItem(0, nearest);
	}
	
	
	
	@EventHandler
	public void onDamage(EntityDamageEvent event) {
		if(event.getEntity() instanceof Player) {
			Player player = (Player) event.getEntity();
			if(!FalconAPI.getAlive().contains(player)) {
				event.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onDamage2(EntityDamageByEntityEvent event) {
		if(event.getDamager() instanceof Player) {
			Player player = (Player) event.getDamager();
			if(!FalconAPI.getAlive().contains(player)) {
				event.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onDrop(PlayerDropItemEvent event) {
		if(!FalconAPI.getAlive().contains(event.getPlayer())) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onBreak(BlockBreakEvent event) {
		if(!FalconAPI.getAlive().contains(event.getPlayer())) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlace(BlockPlaceEvent event) {
		if(!FalconAPI.getAlive().contains(event.getPlayer())) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onEnter(VehicleEnterEvent event) {
		if(event.getEntered() instanceof Player) {
			Player player = (Player) event.getEntered();
			if(!FalconAPI.getAlive().contains(player)) {
				event.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onDestroy(VehicleDamageEvent event) {
		if(event.getAttacker() instanceof Player) {
			Player player = (Player) event.getAttacker();
			if(!FalconAPI.getAlive().contains(player)) {
				event.setCancelled(true);
			}
		}
	}
}
