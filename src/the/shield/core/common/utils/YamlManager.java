package the.shield.core.common.utils;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.YamlConfiguration;

public class YamlManager {
	
	public static YamlConfiguration getConfig(String filePath){
		File file = new File(filePath);
		if(!file.exists()){
			try {
				file.mkdir();
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return YamlConfiguration.loadConfiguration(file);
	}
	
	public static boolean exists(String filePath){
		return new File(filePath).exists();
	}
	
	public static void delete(String filePath){
		new File(filePath).delete();
	}
	
	
}