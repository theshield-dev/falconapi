package the.shield.core.common.utils;

import net.minecraft.server.v1_8_R3.EntityCreature;
import net.minecraft.server.v1_8_R3.Navigation;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftCreature;
import org.bukkit.entity.Creature;

public class UMove {

	  public static boolean CreatureMoveFast(org.bukkit.entity.Entity ent, Location target, float speed)
	  {
	    if (!(ent instanceof Creature)) {
	      return false;
	    }
	    if(ent.getLocation().distance(target) > 7)
	    {
	    	ent.teleport(target);
	    	return false;
	    }
	    EntityCreature ec = ((CraftCreature)ent).getHandle();
	    ec.getControllerMove().a(target.getX(), target.getY(), target.getZ(), speed);
	    
	    return true;
	  }
	  
	  public static void CreatureMove(org.bukkit.entity.Entity ent, Location target, float speed)
	  {
	    if (!(ent instanceof Creature)) {
	      return;
	    }
	    if (UtilMath.offset(ent.getLocation(), target) < 0.1D) {
	      return;
	    }
	    EntityCreature ec = ((CraftCreature)ent).getHandle();
	    Navigation nav = (Navigation) ec.getNavigation();
	    if (UtilMath.offset(ent.getLocation(), target) > 24.0D)
	    {
	      Location newTarget = ent.getLocation();
	      
	      newTarget.add(UtilAlg.getTrajectory(ent.getLocation(), target).multiply(24));
	      
	      nav.a(newTarget.getX(), newTarget.getY(), newTarget.getZ(), speed);
	    }
	    else
	    {
	      nav.a(target.getX(), target.getY(), target.getZ(), speed);
	    }
	  }
}
