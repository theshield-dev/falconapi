package the.shield.core.common.utils;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.tedthetechie.falcon.Falcon;

public class BungeeCord {

	
	public static void connect(org.bukkit.entity.Player player, String servername)
	{
	    Falcon.getPlugin().getServer().getMessenger().registerOutgoingPluginChannel(Falcon.getPlugin(), "BungeeCord");
		ByteArrayDataOutput os = ByteStreams.newDataOutput();
		os.writeUTF("Connect");
		os.writeUTF(servername);
		player.sendPluginMessage(Falcon.getPlugin(), "BungeeCord", os.toByteArray());
	}
}
