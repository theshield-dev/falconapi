package the.shield.core.common.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;



public class UUIDFetcher implements Listener {
	
	private static String HTTP_REQUEST = "https://minespy.net/api/uuid/";
	public static Map<String, String> data = new HashMap<String, String>();
	
	public static UUID getUUIDOf(String player) throws Exception
	{
		if(Bukkit.getPlayer(player) != null)
		{
			return Bukkit.getPlayer(player).getUniqueId();
		} else 
		if(data.containsKey(player.toLowerCase()))
		{
			return getUUID(data.get(player.toLowerCase()));
		} else {
			String request = getRequest(player.toLowerCase());
			data.put(player.toLowerCase(), request);
			return getUUID(request);
		}
	}
	
	public static void reload() {
		for(Player p : Bukkit.getOnlinePlayers()) {
			String key = p.getName().toLowerCase();
			String val = p.getUniqueId().toString();
			if(!data.containsKey(key)) {
				data.put(key, val);
			}
		}
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event)
	{
		logPlayer(event.getPlayer());
	}
	
	public static void logPlayer(Player player)
	{
		if(!(data.containsKey(player.getName())))
		{
			data.put(player.getName(), player.getUniqueId().toString());
		}
	}
	
    private static UUID getUUID(String eid) {
    	String id = eid.replaceAll("-", "");
        return UUID.fromString(id.substring(0, 8) + "-" + id.substring(8, 12) + "-" + id.substring(12, 16) + "-" + id.substring(16, 20) + "-" +id.substring(20, 32));
    }
	
	 
	 public static String getRequest(String player)
	 {
        try {
            URL url = new URL(HTTP_REQUEST + player + "/clear");
            InputStream is = url.openStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            while((line = br.readLine()) != null)
                return line;
            br.close();
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return null;
	 }

}
